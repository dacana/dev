FROM ubuntu

MAINTAINER devopspoints

RUN apt-get update

RUN apt-get install -y vim wget dialog net-tools nano

RUN apt-get install -y nginx

RUN echo "daemon off;" >> /etc/nginx/nginx.conf

COPY run.sh /run.sh
RUN chmod 777 -R /run.sh

EXPOSE 80


CMD bash -C '/run.sh';'bash'